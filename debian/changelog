bibledit-cloud (5.1.023-1) unstable; urgency=medium

  * New upstream version 5.1.023
  * d/control: No longer depend on outdated libgumbo

 -- Teus Benschop <teusbenschop@debian.org>  Thu, 30 Jan 2025 15:44:08 +0100

bibledit-cloud (5.1.022-1) unstable; urgency=medium

  * New upstream version 5.1.022, closes: ##1074326
  * Remove patch build deps, closes: #1077265
  * Remove libcurl build deps, closes: #1077263
  * Remove dep on essential packages tar and gzip

 -- Teus Benschop <teusbenschop@debian.org>  Thu, 02 Jan 2025 12:05:44 +0100

bibledit-cloud (5.1.021-1) unstable; urgency=medium

  * debian/control: Depends: add these: zip, unzip, git, gzip, tar
  * New upstream version 5.1.021

 -- Teus Benschop <teusbenschop@debian.org>  Fri, 13 Dec 2024 16:54:35 +0100

bibledit-cloud (5.1.020-1) unstable; urgency=medium

  * New upstream version 5.1.020

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 05 Oct 2024 18:43:55 +0200

bibledit-cloud (5.1.016-2) unstable; urgency=medium

  * One of the build deps is libcurl-dev. 
    Package libcurl-dev other build deps: 
    libidn2-dev libssh2-1-dev librtmp-dev 
    These three dev packags were added to the build deps.
    This is weird, yes,
  * Remove build dep on libtidy-dev, closes: #1075805

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 27 Jul 2024 15:00:20 +0200

bibledit-cloud (5.1.016-1) unstable; urgency=medium

  * New upstream version 5.1.016
  * Fixes a FTBFS on GCC 14 see bug #1074845
  * Bump Standards-Version

 -- Teus Benschop <teusbenschop@debian.org>  Thu, 04 Jul 2024 08:58:15 +0200

bibledit-cloud (5.1.015-1) unstable; urgency=medium

  * New upstream version 5.1.015
  * Prepare for MbedTLS 2.x -> 3.x transition.

 -- Teus Benschop <teusbenschop@debian.org>  Thu, 27 Jun 2024 17:24:17 +0200

bibledit-cloud (5.1.013-1) unstable; urgency=medium

  * New upstream version 5.1.013
  * closes: #1070273

 -- Teus Benschop <teusbenschop@debian.org>  Fri, 03 May 2024 13:14:57 +0200

bibledit-cloud (5.1.012-1) unstable; urgency=medium

  * New upstream version 5.1.012

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 27 Apr 2024 17:11:58 +0200

bibledit-cloud (5.1.005-1) unstable; urgency=medium

  * New upstream version 5.1.005
  * Should build on architecture hurd-i386.

 -- Teus Benschop <teusbenschop@debian.org>  Fri, 04 Aug 2023 18:49:06 +0200

bibledit-cloud (5.1.002-1) unstable; urgency=medium

  * New upstream version 5.1.002

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 17 Jun 2023 15:21:58 +0200

bibledit-cloud (5.0.992-4) unstable; urgency=medium

  * d/control: Depend on libpugixml-dev

 -- Teus Benschop <teusbenschop@debian.org>  Wed, 07 Dec 2022 20:03:44 +0100

bibledit-cloud (5.0.992-3) unstable; urgency=medium

  * Vcs-Git: fix lintian warning

 -- Teus Benschop <teusbenschop@debian.org>  Tue, 29 Nov 2022 17:12:30 +0100

bibledit-cloud (5.0.992-2) unstable; urgency=medium

  * d/control: Fix vcs-git

 -- Teus Benschop <teusbenschop@debian.org>  Mon, 28 Nov 2022 17:27:34 +0100

bibledit-cloud (5.0.992-1) unstable; urgency=medium

  * Move quill sources to d/missing-sources
  * New upstream version 5.0.992

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 27 Nov 2022 17:17:54 +0100

bibledit-cloud (5.0.990-3) unstable; urgency=medium

  * d/control: Fix VCS URL

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 27 Nov 2022 10:41:21 +0100

bibledit-cloud (5.0.990-2) unstable; urgency=medium

  * d/control: New repo URL
  * closes: #975407

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 26 Nov 2022 16:25:47 +0100

bibledit-cloud (5.0.990-1) unstable; urgency=medium

  * Add build deps ibgumbo-dev and libtidy-dev
  * New upstream version 5.0.990

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 26 Nov 2022 16:23:38 +0100

bibledit-cloud (5.0.989-1) unstable; urgency=medium

  [ Bastian Germann ]
  * lintian: superfluous-file-pattern
  * quill is implemented in TypeScript
  * lintian: debian-rules-uses-unnecessary-dh-argument
  * lintian: useless-autoreconf-build-depends
  * lintian: unknown-paragraph-in-dep5-copyright

  [ Teus Benschop ]
  * bump standards version
  * New upstream version 5.0.989
  * Fix for bug #1023373

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 06 Nov 2022 19:51:59 +0100

bibledit-cloud (5.0.988-3) unstable; urgency=medium

  * d/control: recommends diatheke

 -- Teus Benschop <teusbenschop@debian.org>  Tue, 18 Oct 2022 18:17:01 +0200

bibledit-cloud (5.0.988-2) unstable; urgency=medium

  * d/control: Add libutf8proc-dev

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 25 Sep 2022 18:17:29 +0200

bibledit-cloud (5.0.988-1) unstable; urgency=medium

  * d/copyright: Fix source
  * d/watch: New URL
  * New upstream version 5.0.988

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 25 Sep 2022 17:11:47 +0200

bibledit-cloud (5.0.983-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Teus Benschop ]
  * Remove failing patches
  * New upstream version 5.0.983

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 17 Jul 2022 16:51:40 +0200

bibledit-cloud (5.0.978+dfsg1-1) unstable; urgency=medium

  * Team upload
  * Switch to debhelper-compat
  * Add d/watch download and d/copyright repack rules
  * New upstream version 5.0.978+dfsg1

 -- Bastian Germann <bage@debian.org>  Sat, 14 May 2022 01:16:07 +0200

bibledit-cloud (5.0.978-1) unstable; urgency=medium

  * New upstream version 5.0.978

 -- Teus Benschop <teusbenschop@debian.org>  Tue, 08 Feb 2022 20:22:41 +0100

bibledit-cloud (5.0.977-1) unstable; urgency=medium

  * New upstream version 5.0.977

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 06 Feb 2022 20:39:09 +0100

bibledit-cloud (5.0.964-3) unstable; urgency=medium

  * Fix Impossible Build-Depends dh-systemd

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 13 Nov 2021 10:13:07 +0100

bibledit-cloud (5.0.964-2) unstable; urgency=medium

  * Fix build error: add dependency: libxml2-dev

 -- Teus Benschop <teusbenschop@debian.org>  Fri, 05 Nov 2021 08:47:41 +0100

bibledit-cloud (5.0.964-1) unstable; urgency=medium

  * lintian overrides
  * New upstream version 5.0.964, closes: #998500
  * Fix lintian error: add dependency dh-autoreconf
  * Fix lintian error: add dependency dh-systemd

 -- Teus Benschop <teusbenschop@debian.org>  Fri, 05 Nov 2021 08:46:57 +0100

bibledit-cloud (5.0.922-1) unstable; urgency=medium

  * new upstream version 5.0.922
  * update Standards version
  * override lintian warnings

 -- Teus Benschop <teusbenschop@debian.org>  Thu, 03 Dec 2020 19:14:06 +0100

bibledit-cloud (5.0.912-1) unstable; urgency=medium

  * New upstream version 5.0.912

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 19 Sep 2020 20:52:53 +0200

bibledit-cloud (5.0.903-1) unstable; urgency=medium

  * New upstream version 5.0.903

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 19 Jul 2020 15:36:56 +0200

bibledit-cloud (5.0.901-1) unstable; urgency=medium

  * New upstream version 5.0.901

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 11 Jul 2020 18:36:54 +0200

bibledit-cloud (5.0.892-1) unstable; urgency=medium

  * New upstream version 5.0.892

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 31 May 2020 13:42:25 +0200

bibledit-cloud (5.0.886-1) unstable; urgency=medium

  * Non-maintainer upload
  * New upstream version 5.0.886
  * d/control: maintainer: XWire Packaging Team

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 25 Apr 2020 18:34:27 +0200

bibledit-cloud (5.0.813-2) unstable; urgency=medium

  * d/compat 10, closes: #950592
  * d/control: delete build dependency autotools-dev
  * d/control: delete build dependency dh-autoreconf
  * d/control: multi-arch: foreign
  * d/control: updated standards-version

 -- Teus Benschop <teusbenschop@debian.org>  Sun, 16 Feb 2020 12:31:34 +0100

bibledit-cloud (5.0.813-1) unstable; urgency=medium

  * New upstream version 5.0.813
  * d/copyright: Update as per FTP masters request

 -- Teus Benschop <teusbenschop@debian.org>  Mon, 03 Feb 2020 19:05:24 +0100

bibledit-cloud (5.0.801-1) unstable; urgency=medium

  * Initial Debian release
  * Complete ITP, closes: #863408

 -- Teus Benschop <teusbenschop@debian.org>  Sat, 26 Oct 2019 12:41:29 +0200

